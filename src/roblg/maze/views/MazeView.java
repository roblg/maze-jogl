package roblg.maze.views;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.fixedfunc.GLMatrixFunc;

import roblg.maze.CurrentCellObserver;
import roblg.maze.Maze;
import roblg.maze.MazeCell;

public class MazeView implements GLEventListener, CurrentCellObserver {
	
	public static int WALL_SIZE_HORIZ = 2; // px
	public static int WALL_SIZE_VERT = 2; // px
	
	static final double[] WALL_COLOR = new double[] { 0.8, 0.0, 0.0 };
	static final double[] BG_COLOR = new double[] { 0.0, 0.0, 0.0 };
	static final double[] START_COLOR = new double[] { 0.0, 0.0, 0.6 };
	static final double[] END_COLOR = new double[] { 0.0, 0.6, 0.0 };
	static final double[] PATH_MARKER_COLOR = new double[] { 0.3, 0.6, 0.0 };
	static final double[] CURRENT_CELL_COLOR = new double[] { 0.3, 0.6, 0.6 };
	
	private final Maze maze;
	
	private int height;
	private int width;
	private double cellWidth;
	private double cellHeight;
	
	private volatile boolean redrawAll;
	
	private List<MazeCell> dirtyList = Collections.synchronizedList(new ArrayList<MazeCell>());
	
	public MazeView(Maze maze) {
		this.maze = maze;
		this.maze.registerCurrentCellListener(this);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		// unclear whether or not I really care, but this 
		// could be called multiple times. 
		
			GL2 gl = drawable.getGL().getGL2();
			
			gl.glPushMatrix(); // required for initSizes to function properly
			
			initSizes(drawable);
			
			this.redrawAll = true;
			
	}

	private void initSizes(GLAutoDrawable drawable) {
		
		// TODO: move this business into initSizes so reshape works properly 
		
		GL2 gl = drawable.getGL().getGL2();
		
		gl.glPopMatrix(); // this is OK because we have a pushMatrix() in init. not ideal, but works
		
		// 2D Projection (I think)
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(0, width, height, 0, 0, 1);
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glDisable(GL.GL_DEPTH_TEST);
	
		gl.glClearColor(0f, 0f, 0f, 0f);
		gl.glTranslated(0.375, 0.375, 0.0); // draw inside pixels
		gl.glPushMatrix();
		
		
		final int cols = maze.getCols();
		final int rows = maze.getRows();
		this.height = drawable.getHeight();
		this.width = drawable.getWidth();
		this.cellWidth = (width - ((cols + 1) * WALL_SIZE_HORIZ)) / (double)cols;
		this.cellHeight = (height - ((rows + 1) * WALL_SIZE_VERT)) / (double)rows;
		
		System.out.format("%d, %d, %.2f, %.2f\n", height, width, cellWidth, cellHeight);		
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		// TODO Auto-generated method stub
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		// System.out.println("DISPLAY");
		drawHelper(drawable);
	}
	
	private void drawHelper(GLAutoDrawable drawable) {
		if (!dirtyList.isEmpty()) {
			System.out.println("drawing dirty");
			MazeCell[] dirtyCopy = new MazeCell[0];
			synchronized (dirtyList) {
				if (!dirtyList.isEmpty()) {
					dirtyCopy = dirtyList.toArray(dirtyCopy);
					dirtyList.clear();
				}
			}
			drawDirty(drawable.getGL().getGL2(), dirtyCopy);
		} else if (redrawAll) {
			redrawAll(drawable);
		}
	}
	
	private void drawDirty(GL2 gl, MazeCell[] dirtyCells) {
		//gl.glClear( GL.GL_COLOR_BUFFER_BIT );
		// draw a triangle filling the window
		gl.glLoadIdentity();
		for (MazeCell mc : dirtyCells) {
			drawCell(gl, mc);
		}
	}
	
	private void redrawAll(GLAutoDrawable drawable) {
		final int cols = maze.getCols();
		final int rows = maze.getRows();
		
		final GL2 gl = drawable.getGL().getGL2();
		
		gl.glClear( GL.GL_COLOR_BUFFER_BIT );

		// draw a triangle filling the window
		gl.glLoadIdentity();
		
		for (int row = 0; row < (2 * rows) + 1; row++) {
			for (int col = 0; col < (2 * cols) + 1; col++) {
				
				if (col % 2 == 1 && row % 2 == 1) {
					int cellX = (col - 1) / 2;
					int cellY = (row - 1) / 2;
					drawCell(gl, maze.getCell(cellX, cellY));
				} else {
					
					if ((col % 2 == 0 && row % 2 == 0)
							|| col == 0 
							|| row == 0 
							|| col == (2*cols) 
							|| row == (2*rows)) {
						// guaranteed to have walls
						drawWall(gl, col, row);
					} else {
						
						// maybe a wall between cells. 
						// figure out what cell col,row is
						int cellLeftCol = (col - 1) / 2;
						int cellLeftRow = (row / 2); // technically also the cellRightRow, but easier to tie it to cellleftX this way
						
						int cellAboveCol = (col / 2);
						int cellAboveRow = (row - 1) / 2;
						
						// the following looks kind of funny, but actually works
						if (col % 2 == 0 && maze.wallBetween(maze.getCell(cellLeftCol, cellLeftRow), maze.getCell(cellLeftCol + 1, cellLeftRow))) {
							// vertical wall
							drawWall(gl, col, row);
						} else if (row % 2 == 0 && maze.wallBetween(maze.getCell(cellAboveCol, cellAboveRow), maze.getCell(cellAboveCol, cellAboveRow + 1))) {
							drawWall(gl, col, row);
						}
					}
					
				}
			}
		}
	}
	
	private double getTranslateXForCell(int cellX) {
		int numCellWidths = cellX; 			 
		int numWallWidths = cellX + 1;
		return (numCellWidths * cellWidth) + (numWallWidths * WALL_SIZE_HORIZ);
	}
	
	private double getTranslateYForCell(int cellY) {
		int numCellHeights = cellY;
		int numWallHeights = cellY + 1;
		return (numCellHeights * cellHeight) + (numWallHeights * WALL_SIZE_VERT);
	}
	
	private void drawCell(GL2 gl, MazeCell cell) {
		gl.glPushMatrix();
		
		gl.glTranslated(getTranslateXForCell(cell.getColumn()), getTranslateYForCell(cell.getRow()), 0);
		
		// clear the cell
		gl.glColor3dv(BG_COLOR, 0);
		drawRect(gl, cellWidth, cellHeight);
		
		gl.glPushMatrix();
		// definitely in a cell.
		if (cell.equals(maze.getCurrent())) {
			gl.glTranslated(cellWidth / 8, cellHeight / 8, 0);
			gl.glColor3dv(CURRENT_CELL_COLOR, 0);
			drawRect(gl, cellWidth * .75, cellHeight * .75);
		} else if (cell.equals(maze.getStart())) {
			gl.glTranslated(cellWidth / 8, cellHeight / 8, 0);
			gl.glColor3dv(START_COLOR, 0);
			drawRect(gl, cellWidth * .75, cellHeight * .75);
		} else if (cell.equals(maze.getEnd())) {
			gl.glTranslated(cellWidth / 8, cellHeight / 8, 0);
			gl.glColor3dv(END_COLOR, 0);
			drawRect(gl, cellWidth * .75, cellHeight * .75);
		} else if (maze.onPath(cell)) {
			gl.glTranslated(cellWidth * .375, cellHeight * .375, 0); // 3/8 of the way in
			gl.glColor3dv(PATH_MARKER_COLOR, 0);
			drawRect(gl, cellWidth * .25, cellHeight * .25);
		}
		gl.glPopMatrix();
		
		gl.glPopMatrix();
	}
	
	private void drawWall(GL2 gl, int col, int row) {
		gl.glPushMatrix();
		
		gl.glTranslated(getTranslateXForWall(col), getTranslateYForWall(row), 0);
		
		double width = (col % 2 == 1) ? cellWidth : WALL_SIZE_HORIZ;
		double height = (row % 2 == 1) ? cellHeight : WALL_SIZE_VERT;
		gl.glColor3dv(WALL_COLOR, 0);
		drawRect(gl, width, height);
		gl.glPopMatrix();
	}
	
	private double getTranslateXForWall(int wallX) {
		int numCellWidths = wallX / 2; 			// 0 1 2 3 4 5 --> 0 0 1 1 2
		int numWallWidths = (wallX + 1) / 2; 	// 0 1 2 3 4 5 --> 0 1 1 2 2
		return (numCellWidths * cellWidth) + (numWallWidths * WALL_SIZE_HORIZ);
	}
	
	private double getTranslateYForWall(int wallY) {
		int numCellHeights = wallY / 2;
		int numWallHeights = (wallY + 1) / 2;
		return (numCellHeights * cellHeight) + (numWallHeights * WALL_SIZE_VERT);
	}
	
	private void drawRect(GL2 gl, double width, double height) {
		gl.glBegin(GL2.GL_QUADS);
		gl.glVertex2d(0.0, 0.0);
		gl.glVertex2d(0.0, height);
		gl.glVertex2d(width, height);
		gl.glVertex2d(width, 0.0);
		gl.glEnd();
	}

	/**
	 * {@inheritDoc}
	 * @param drawable
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		
		System.out.println("RESHAPE");
		this.redrawAll = true;
		initSizes(drawable);
		drawHelper(drawable);
	}

	@Override
	public void onCurrentCellChange(MazeCell prev, MazeCell current) {
		synchronized(this.dirtyList) {
			this.dirtyList.add(prev);
			this.dirtyList.add(current);
		}
	}
	
}
