package roblg.maze;

public interface MazeEndObserver {
	public void onMazeEnd();
}
