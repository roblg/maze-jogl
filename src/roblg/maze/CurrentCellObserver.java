package roblg.maze;

public interface CurrentCellObserver {
	void onCurrentCellChange(MazeCell prev, MazeCell current);
}
