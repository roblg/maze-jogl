package roblg.maze;

public class MazeCell {

	private int column;
	private int row;
	private boolean isStart;
	private boolean isEnd;
	
	private MazeCell top;
	private MazeCell right;
	private MazeCell bottom;
	private MazeCell left;
	
	public MazeCell(int col, int row) {
		this.column = col;
		this.row = row;
	}
	
	public int getColumn() {
		return column;
	}
	public MazeCell setColumn(int column) {
		this.column = column;
		return this;
	}
	public int getRow() {
		return row;
	}
	public MazeCell setRow(int row) {
		this.row = row;
		return this;
	}
	public boolean getIsStart() {
		return isStart;
	}
	public MazeCell setIsStart(boolean isStart) {
		this.isStart = isStart;
		return this;
	}
	public boolean getIsEnd() {
		return isEnd;
	}
	public MazeCell setIsEnd(boolean isEnd) {
		this.isEnd = isEnd;
		return this;
	}
	public MazeCell getTop() {
		return top;
	}
	public void setTop(MazeCell top) {
		this.top = top;
	}
	public MazeCell getBottom() {
		return bottom;
	}
	public void setBottom(MazeCell bottom) {
		this.bottom = bottom;
	}
	public MazeCell getRight() {
		return right;
	}
	public void setRight(MazeCell right) {
		this.right = right;
	}
	public MazeCell getLeft() {
		return left;
	}
	public void setLeft(MazeCell left) {
		this.left = left;
	}
	
	@Override
	public String toString() {
		return String.format("{MazeCell: (%d,%d)}", getColumn(), getRow());
	}
	
}
