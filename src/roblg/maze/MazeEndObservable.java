package roblg.maze;

public interface MazeEndObservable {
	public void registerMazeEndObserver(MazeEndObserver observer);
}
