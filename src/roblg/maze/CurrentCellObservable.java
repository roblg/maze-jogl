package roblg.maze;

public interface CurrentCellObservable {
	void registerCurrentCellListener(CurrentCellObserver observer);
}
