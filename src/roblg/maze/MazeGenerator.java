package roblg.maze;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class MazeGenerator {

	public static Maze generateDFS(int cols, int rows) {
		
		final Random r = new Random();
		
		MazeCell[][] mazeCells = new MazeCell[cols][rows];
		for (int col = 0; col < mazeCells.length; col++) {
			for (int row = 0; row < mazeCells[col].length; row++) {
				mazeCells[col][row] = new MazeCell(col, row);
			}
		}
		
		MazeCell start = mazeCells[r.nextInt(cols)][rows-1].setIsStart(true);
		MazeCell end = mazeCells[r.nextInt(cols)][0].setIsEnd(true);
		
		Set<MazeCell> visited = new HashSet<MazeCell>();
		generateHelper(mazeCells, visited, start, r);
				
		return new Maze(mazeCells, start, end);
		
	}
	
	private static void generateHelper(MazeCell[][] cellData, Set<MazeCell> visited, MazeCell currentCell, Random r) {
		
		visited.add(currentCell);
		
		while (true) {
			List<MazeCell> nbrs = getUnvisitedNeighbors(cellData, visited, currentCell);
			if (nbrs.isEmpty()) {
				break;
			}
			
			MazeCell randomNeighbor = nbrs.get(r.nextInt(nbrs.size()));
			removeWall(currentCell, randomNeighbor);
			generateHelper(cellData, visited, randomNeighbor, r);
		}
	}
	
	private static void removeWall(MazeCell one, MazeCell two) {
		int colDiff = two.getColumn() - one.getColumn();
		int rowDiff = two.getRow() - one.getRow();
		if (rowDiff == 0 && colDiff == 1) {
			one.setRight(two);
			two.setLeft(one);
		} else if (rowDiff == 0 && colDiff == -1) {
			one.setLeft(two);
			two.setRight(one);			
		} else if (rowDiff == 1 && colDiff == 0) {
			one.setBottom(two);
			two.setTop(one);
		} else if (rowDiff == -1 && colDiff == 0) {
			one.setTop(two);
			two.setBottom(one);
		} else {
			throw new IllegalArgumentException("MazeCells are not adjacent");
		}
	}
	
	private static List<MazeCell> getUnvisitedNeighbors(MazeCell[][] cellData, Set<MazeCell> visited, MazeCell currentCell) {
		List<MazeCell> nbrs = getPossibleNeighbors(cellData, currentCell);
		
		Iterator<MazeCell> it = nbrs.iterator();
		while (it.hasNext()) {
			if (visited.contains(it.next())) {
				it.remove();
			}
		}
		return nbrs;
	}
	
	private static List<MazeCell> getPossibleNeighbors(MazeCell[][] cellData, MazeCell currentCell) {
		int col = currentCell.getColumn();
		int row = currentCell.getRow();
		
		List<MazeCell> nbrs = new ArrayList<MazeCell>();
		if (col > 0) { 
			nbrs.add(cellData[col - 1][row]); 
		}
		if (col + 1 < cellData.length) { 
			nbrs.add(cellData[col + 1][row]); 
		}
		if (row > 0) { 
			nbrs.add(cellData[col][row - 1]); 
		}
		if (row + 1 < cellData[0].length) { 
			nbrs.add(cellData[col][row + 1]); 
		}
		
		return nbrs;
		
	}
	
}
