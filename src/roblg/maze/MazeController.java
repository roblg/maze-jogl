package roblg.maze;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import roblg.maze.Maze.Direction;
import roblg.maze.views.MazeView;


public class MazeController implements KeyListener, MazeEndObserver {

	static {
        GLProfile.initSingleton( );
    }

	private final Maze maze;
	
	public MazeController(Maze m) {
		this.maze = m;
		this.maze.registerMazeEndObserver(this);
	}

	@Override
	public void keyPressed(KeyEvent event) {
		// System.out.println("KEYPRESS: " + event.getKeyCode() + " " + KeyEvent.VK_LEFT);
		switch (event.getKeyCode()) {
		case KeyEvent.VK_UP:
			maze.move(Direction.UP);
			break;
		case KeyEvent.VK_RIGHT:
			maze.move(Direction.RIGHT);
			break;
		case KeyEvent.VK_DOWN:
			maze.move(Direction.DOWN);
			break;
		case KeyEvent.VK_LEFT:
			maze.move(Direction.LEFT);
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMazeEnd() {
		System.out.println("YOU WIN!");
	}
	
	public void show() {
		GLProfile glProfile = GLProfile.getDefault();
		GLCapabilities glCapabilities = new GLCapabilities(glProfile);
		
		final GLCanvas canvas = new GLCanvas(glCapabilities);
		
		MazeView mv = new MazeView(this.maze);
		canvas.addGLEventListener(mv);
		canvas.addKeyListener(this);
		canvas.setSize(1200, 800);
		
		final ExecutorService es = Executors.newSingleThreadExecutor();

		Runnable updater = new Runnable() {
			@Override
			public void run() {
				// TODO: what's the best practice for this?
				while (true) {
					canvas.repaint();
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) { /* squelch */ }
				}
			}			
		};
		
		final JFrame jframe = new JFrame( "OpenGL Maze" ); 
        jframe.addWindowListener( new WindowAdapter() {
            public void windowClosing( WindowEvent windowevent ) {
            	es.shutdown();
                jframe.dispose();
                System.exit( 0 );
            }
        });
        

        // run updater thread
        es.submit(updater);
        
        jframe.getContentPane().add( canvas, BorderLayout.CENTER );
        jframe.setSize( 1200, 800);
       //  jframe.setResizable(false);
        jframe.setVisible( true );
        
        canvas.requestFocus();
	}
	
	public static void main(String[] args) {
		
		Maze m = MazeGenerator.generateDFS(12, 8);
		MazeController mc = new MazeController(m);
		mc.show();

	}

}
