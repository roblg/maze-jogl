package roblg.maze;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

public class Maze implements Iterable<MazeCell>, CurrentCellObservable, MazeEndObservable {

	private MazeCell[][] mazeCells;
	private MazeCell start;
	private MazeCell end;
	
	private LinkedHashSet<MazeCell> path = new LinkedHashSet<MazeCell>();
	private MazeCell current;
	
	private List<CurrentCellObserver> currentCellObservers = new ArrayList<CurrentCellObserver>();
	private List<MazeEndObserver> mazeEndObservers = new ArrayList<MazeEndObserver>();
	
	public Maze(MazeCell[][] mazeCells, MazeCell start, MazeCell end) {
		super();
		this.mazeCells = mazeCells;
		this.current = this.start = start;
		this.end = end;
	}	
	
	public MazeCell getStart() {
		return start;
	}
	public MazeCell getEnd() {
		return end;
	}
	public int getCols() {
		return mazeCells.length;
	}
	public int getRows() {
		return mazeCells[0].length;
	}
	
	public MazeCell getCell(int col, int row) {
		return mazeCells[col][row];
	}
	
	public boolean wallBetween(MazeCell a, MazeCell b) {
		return !equal(a.getTop(), b) && !equal(a.getRight(), b) && !equal(a.getBottom(), b) && !equal(a.getLeft(), b);
	}
	
	private static boolean equal(MazeCell a, MazeCell b) {
		return (null == a && null == b) || null != a && a.equals(b);
	}
	
	public MazeCell getCurrent() {
		return current;
	}

	private MazeCell updateCurrent(MazeCell newCurrent) {
		if (null != newCurrent) {
			MazeCell prev = this.current;
			if (onPath(newCurrent)) {
				// we're backtracking
				this.path.remove(prev);
			} else {
				this.path.add(newCurrent);
			}
			this.current = newCurrent;
			this.notifyCurrentCellObservers(prev);
			
			if (getCurrent().equals(getEnd())) {
				this.notifyMazeEndObservers();
			}
			
			return getCurrent();
		}
		return null;
	}

	public Iterable<MazeCell> getPath() {
		return Collections.unmodifiableSet(path);
	}
	
	public boolean onPath(MazeCell cell) {
		return path.contains(cell);
	}

	public static enum Direction {
		UP, RIGHT, DOWN, LEFT
	}
	
	public MazeCell move(Direction dir) {
		if (getCurrent().equals(getEnd())) {
			return null;
		}
		
		switch (dir) {
		case UP:
			return this.updateCurrent(getCurrent().getTop());
		case RIGHT:
			return this.updateCurrent(getCurrent().getRight());
		case DOWN:
			return this.updateCurrent(getCurrent().getBottom());
		case LEFT:
			return this.updateCurrent(getCurrent().getLeft());
		default:
			throw new IllegalArgumentException("Unknown Direction: " + dir);
		}
	}
	
	@Override
	public Iterator<MazeCell> iterator() {
		return new MazeCellIterator();
	}
	
	private class MazeCellIterator implements Iterator<MazeCell> {

		private int count = 0;
		
		@Override
		public boolean hasNext() {
			return count != (getCols() * getRows());
		}

		@Override
		public MazeCell next() {
			int col = count % getCols();
			int row = count / getCols();
			MazeCell ret = mazeCells[col][row];
			count++;
			return ret;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
		
	}

	@Override
	public void registerCurrentCellListener(CurrentCellObserver observer) {
		this.currentCellObservers.add(observer);
	}
	
	private void notifyCurrentCellObservers(MazeCell prev) {
		for (CurrentCellObserver observer : this.currentCellObservers) {
			observer.onCurrentCellChange(prev, getCurrent());
		}
	}

	@Override
	public void registerMazeEndObserver(MazeEndObserver observer) {
		this.mazeEndObservers.add(observer);	
	}
	
	private void notifyMazeEndObservers() {
		for (MazeEndObserver observer : this.mazeEndObservers) {
			observer.onMazeEnd();
		}
	}
	
	// TODO: observer methods -- on move, on success
	// TODO: move method 
	
}
