
# Setup #

I would like this to be more point-and-click, but jogamp chose to use 7-zip for their archives, which makes life a bit more complicated for command-line scripts.

* Clone this git repository
* Download JOGL from http://jogamp.org/deployment/jogamp-current/archive/jogamp-all-platforms.7z
* Unzip it using a program like 7-zip (OSX Archive utility seems to work too, I think)
* cd to the project root, and run ./setup-jogl-dependencies.sh <path to your extracted jogamp>. This will create symlinks to the appropriate jar files.
* Import the project into Eclipse