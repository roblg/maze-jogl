#!/bin/bash

# This assumes that it is being run from the project root.
# usage: "./setup-jogl-dependencies.sh <relative path to unzipped jogl archive>"

if [ $# -ne 1 ]; then
	echo "Usage: ./setup-jogl-dependencies.sh <path to unzipped jogl archive>"
	exit 1
fi

mkdir -p lib

JOGL_PATH="$1"

pushd lib/ > /dev/null

ln -s ../$JOGL_PATH/jar/jogl-all.jar jogl-all.jar
ln -s ../$JOGL_PATH/jar/gluegen-rt.jar gluegen-rt.jar
ln -s ../$JOGL_PATH/jogl-java-src.zip jogl-java-src.zip
ln -s ../$JOGL_PATH/gluegen-java-src.zip gluegen-java-src.zip

popd > /dev/null

echo "Done"
